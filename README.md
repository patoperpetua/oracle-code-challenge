# Oracle Code Challenge - 1800

----------------------

This project contains the code challenge solution proposed for an interview at Oracle Australia.

## Stack

- JDK 11
- Linux / MacOS
- IntelliJ (Optional)

## Why did I select this problem

To be honest, both problems sound challenging but there is more information regarding the phone number challenge. Sometimes when facing problems to solve, the availability of information on the internet could be used as a safety net in case the difficulty (assessed at the beginning) increases, and the problem became unsolvable. 

On the other hand, I felt I could split the different requirements into small changes that can be applied in iteration cycles. 

## Design and approach

To achieve a result, I've sliced the document in small stories that can be independent to each other. Under this link you can find a Trello board of the different stories and how they were linked to be each requirement.
The design was based on constructing several services with a defined task that can be combined and resolve this specific issue.

Having small and specific services allowed me to generate simple and straightforward unit test which improves the quality of the code developed. 

Follow [this link](https://trello.com/invite/b/XfsH6s59/604e7c7fc396bd8cf6d6db23db7dd66e/oracle-code-challenge) to see the project management plan applied.

## Problem description

Follow [this link](docs/Oracle%20-%201800%20CODING%20CHALLENGE.pdf) to see the document provided with the problem specifications and requirements.

## How to run

You can execute the project using your favourite IDE by selecting the main file: *com.singletonsd.oraclecodechallenge.OracleCodeChallengeApplication*

If you want to generate the jar file, execute:
```bash
mvn package
```

Go to the target folder and execute
```bash
java -jar oracle-code-challenge-1.jar
```

By default, it will ask for a phone number, or you can use the help option to know more about the available arguments.

Available options:
```bash
usage: utility-name
 -d,--dictionary <arg>   dictionary file path
 -f,--file <arg>         phone numbers file path
 -h,--help               available options
```

You can do a quick test by executing the following command:

```bash
java -jar oracle-code-challenge-1.jar --file=../src/test/resources/files/phone-test.txt 
```

## Screenshots

### Reading phone number file
![](screenshots/Patricio%20Perpetua%20-%20Oracle%20Code%20Challenge%20-%20File%20Mode.png)

### STDIN Mode
![](screenshots/Patricio%20Perpetua%20-%20Oracle%20Code%20Challenge%20-%20STDIN%20Mode.png)

## Useful Links

- [Spring Boot Console Application by baeldung](https://www.baeldung.com/spring-boot-console-app)
- [Spring Initializr](https://start.spring.io/)
- [Print all possible words from phone digits](https://www.geeksforgeeks.org/find-possible-words-phone-digits/)
- [Solution: Letter Combinations of a Phone Number](https://dev.to/seanpgallivan/solution-letter-combinations-of-a-phone-number-1n91)
- [Depth First Search Algorithm](https://en.wikipedia.org/wiki/Depth-first_search)
- [Github Dwyl - List Of English Words](https://github.com/dwyl/english-words)
- [Java – Reading a Large File Efficiently](https://www.novixys.com/blog/java-reading-large-file-efficiently/)
- [Word to Phone Number Converter](https://miniwebtool.com/word-to-phone-number-converter)
----------------------

© [Patricio Perpetua](http://patricioperpetua.com.au), Australia, 2021.
