package com.singletonsd.oraclecodechallenge;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.system.CapturedOutput;
import org.springframework.boot.test.system.OutputCaptureExtension;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(args ={"--file=src/test/resources/files/phone-test.txt"}, classes = OracleCodeChallengeApplication.class )
@ExtendWith(OutputCaptureExtension.class)
class OracleCodeChallengeApplicationIT {

	@Test
	void shouldPrintTheGivenNames(CapturedOutput output) {
		assertThat(output).contains("CALVIN");
		assertThat(output).contains("ORACLE");
		assertThat(output).contains("PATRICIO");
	}

}
