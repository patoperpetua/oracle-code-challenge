package com.singletonsd.oraclecodechallenge.services;

import com.singletonsd.oraclecodechallenge.exceptions.NotANumberException;
import com.singletonsd.oraclecodechallenge.exceptions.OutOfBoundNumberException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { PhoneDigitTranslateService.class })
public class PhoneDigitTranslateServiceTest {

    @Autowired
    private PhoneDigitTranslateService subject;

    @Test
    public void shouldReturnLettersFromChar() throws NotANumberException {
        String result = subject.translatePhoneDigit('3');
        assertThat(result.length()).isEqualTo(3);
        assertThat(result.charAt(0)).isEqualTo('D');
        assertThat(result.charAt(1)).isEqualTo('E');
        assertThat(result.charAt(2)).isEqualTo('F');
    }

    @Test
    public void shouldReturnLettersFromInt() throws OutOfBoundNumberException {
        String result = subject.translatePhoneDigit(6);
        assertThat(result.length()).isEqualTo(3);
        assertThat(result.charAt(0)).isEqualTo('M');
        assertThat(result.charAt(1)).isEqualTo('N');
        assertThat(result.charAt(2)).isEqualTo('O');
    }

    @Test
    public void shouldReturnLettersFromString() throws NotANumberException {
        List<String> result = subject.translatePhoneDigit("36");
        assertThat(result.size()).isEqualTo(2);
        assertThat(result.get(0).charAt(0)).isEqualTo('D');
        assertThat(result.get(0).charAt(1)).isEqualTo('E');
        assertThat(result.get(0).charAt(2)).isEqualTo('F');
        assertThat(result.get(1).charAt(0)).isEqualTo('M');
        assertThat(result.get(1).charAt(1)).isEqualTo('N');
        assertThat(result.get(1).charAt(2)).isEqualTo('O');
    }

    @Test
    public void shouldReturnExceptionOutOfBoundNumberException() {
        assertThrows(OutOfBoundNumberException.class, () -> subject.translatePhoneDigit(10));
    }

    @Test
    public void shouldReturnExceptionNotANumberException() {
        assertThrows(NotANumberException.class, () -> subject.translatePhoneDigit("ab"));
    }
}
