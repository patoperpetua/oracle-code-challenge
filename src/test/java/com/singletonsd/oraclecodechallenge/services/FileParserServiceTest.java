package com.singletonsd.oraclecodechallenge.services;

import com.singletonsd.oraclecodechallenge.exceptions.FileNotFoundException;
import com.singletonsd.oraclecodechallenge.exceptions.FileTooLargeException;
import com.singletonsd.oraclecodechallenge.exceptions.NotAFileException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import java.util.List;
import java.util.Objects;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { FileParserService.class })
public class FileParserServiceTest {

    @Autowired
    private FileParserService subject;

    @Test
    public void shouldReturnListWith3Items() throws Exception {
        String resourceName = "files/3-lines-file.txt";
        ClassLoader classLoader = getClass().getClassLoader();

        List<String> list = subject.parseFile(Objects.requireNonNull(classLoader.getResource(resourceName)).getFile());
        assertThat(list.size()).isEqualTo(3);
    }

    @Test
    public void shouldReturnExceptionNotFoundWhenNull() {
        assertThrows(FileNotFoundException.class, () -> subject.parseFile(null));
    }

    @Test
    public void shouldReturnExceptionNotFound() {
        assertThrows(FileNotFoundException.class, () -> subject.parseFile("testfile.txt"));
    }

    @Test
    public void shouldReturnExceptionFileTooLarge() {
        String resourceName = "src/test/resources/files/large-file.txt";
        assertThrows(FileTooLargeException.class, () -> subject.parseFile(resourceName));
    }

    @Test
    public void shouldReturnExceptionNotAFile() {
        String resourceName = "src/test/resources/files";
        assertThrows(NotAFileException.class, () -> subject.parseFile(resourceName));
    }

}
