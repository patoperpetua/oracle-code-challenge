package com.singletonsd.oraclecodechallenge.services;

import com.singletonsd.oraclecodechallenge.models.CLIArguments;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ArgumentParserService.class })
public class ArgumentParserServiceTest {

    @Autowired
    private ArgumentParserService subject;

    @Test
    public void shouldReturnEmptyArguments() {
        assertThat(subject.parse(null)).isEqualTo(new CLIArguments(null, null));
    }

    @Test
    public void shouldReturnHelp() {
        assertThat(subject.parse(new String[]{"-h"})).isEqualTo(null);
        assertThat(subject.parse(new String[]{"--help"})).isEqualTo(null);
    }

    @Test
    public void shouldReturnProperArguments() {
        assertThat(subject.parse(new String[]{"-d=dic.csv", "-f=test.csv"}))
                .isEqualTo(new CLIArguments("dic.csv", "test.csv"));
        assertThat(subject.parse(new String[]{"--dictionary=dic.csv", "-file=test.csv"}))
                .isEqualTo(new CLIArguments("dic.csv", "test.csv"));
    }
}
