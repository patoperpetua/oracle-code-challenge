package com.singletonsd.oraclecodechallenge.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { LetterCombinationService.class })
public class LetterCombinationServiceTest {

    @Autowired
    private LetterCombinationService subject;

    @Test
    public void shouldReturn3Items() {
        List<String> result = subject.generateCombinations(Collections.singletonList("ABC"));
        assertThat(result.size()).isEqualTo(3);
        assertThat(result.get(0)).isEqualTo("A");
        assertThat(result.get(1)).isEqualTo("B");
        assertThat(result.get(2)).isEqualTo("C");
    }

    @Test
    public void shouldReturnMultipleItems() {
        List<String> result = subject.generateCombinations(Arrays.asList("ABC", "DEF"));
        assertThat(result.size()).isEqualTo(9);
        assertThat(result.get(0)).isEqualTo("AD");
        assertThat(result.get(1)).isEqualTo("AE");
        assertThat(result.get(2)).isEqualTo("AF");
        assertThat(result.get(3)).isEqualTo("BD");
        assertThat(result.get(4)).isEqualTo("BE");
        assertThat(result.get(5)).isEqualTo("BF");
        assertThat(result.get(6)).isEqualTo("CD");
        assertThat(result.get(7)).isEqualTo("CE");
        assertThat(result.get(8)).isEqualTo("CF");
    }

    @Test
    public void shouldReturnEmptyWhenNull() {
        List<String> result = subject.generateCombinations(null);
        assertThat(result).isNotNull();
        assertThat(result).isEmpty();
    }

    @Test
    public void shouldReturnEmptyWhenEmpty() {
        List<String> result = subject.generateCombinations(Collections.singletonList(""));
        assertThat(result).isNotNull();
        assertThat(result).isEmpty();
    }
}
