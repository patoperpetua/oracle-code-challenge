package com.singletonsd.oraclecodechallenge.services;

import com.singletonsd.oraclecodechallenge.exceptions.FileNotFoundException;
import com.singletonsd.oraclecodechallenge.exceptions.NotAFileException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { DictionaryService.class })
public class DictionaryServiceTest {

    @Autowired
    private DictionaryService subject;

    @Test
    public void shouldReturnListWith11Items() throws IOException {
        List<String> list = subject.getContainingWords("calvin");
        assertThat(list.size()).isEqualTo(11);
    }

    @Test
    public void shouldReturnListWith11ItemsUpperCase() throws IOException {
        List<String> list = subject.getContainingWords("CALVIN");
        assertThat(list.size()).isEqualTo(11);
    }

    @Test
    public void shouldReturnListWith23Items() throws IOException {
        List<String> list = subject.getContainingWords(Arrays.asList("calvin", "calva"));
        assertThat(list.size()).isEqualTo(23);
    }

    @Test
    public void shouldReturnListWithNoItems() throws IOException, FileNotFoundException, NotAFileException {
        subject.readDictionary("src/test/resources/files/small-dictionary.txt");
        List<String> list = subject.getContainingWords("calvin");
        assertThat(list.size()).isEqualTo(0);
        subject.setDefaultDictionary();
    }

    @Test
    public void shouldReturnListWith1Item() throws IOException {
        List<String> list = subject.getMatchingWords("calvin");
        assertThat(list.size()).isEqualTo(1);
    }

    @Test
    public void shouldNotReturnException() throws FileNotFoundException, NotAFileException {
        String resourceName = "src/test/resources/files/3-lines-file.txt";
        subject.readDictionary(resourceName);
    }

    @Test
    public void shouldReturnExceptionNotFound() {
        assertThrows(FileNotFoundException.class, () -> subject.readDictionary("testfile.txt"));
    }

    @Test
    public void shouldReturnExceptionNotAFile() {
        String resourceName = "src/test/resources/files";
        assertThrows(NotAFileException.class, () -> subject.readDictionary(resourceName));
    }

}
