package com.singletonsd.oraclecodechallenge.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TextSanitizerService.class })
public class TextSanitizerServiceTest {

    @Autowired
    private TextSanitizerService subject;

    @Test
    public void shouldReturnOnlyLetters() {
        assertThat(subject.sanitizeText("123456String - is a squence of chars:~!@#$%^&*(). Test."))
                .isEqualTo("STRINGISASQUENCEOFCHARSTEST");
    }

    @Test
    public void shouldReturnOnlyNumbers() {
        assertThat(subject.sanitizeNumber("123456String - is a squence of chars:~!@#$%^&*(). Test."))
                .isEqualTo("123456");
    }
}
