package com.singletonsd.oraclecodechallenge;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
public class OracleCodeChallengeApplication {

	public static void main(String[] args) {
		log.info("STARTING Oracle Code Challenge");
		SpringApplication.run(CommandLineImplementation.class, args);
		log.info("APPLICATION FINISHED");
	}
}
