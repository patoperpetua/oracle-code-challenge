package com.singletonsd.oraclecodechallenge.models;

import lombok.Data;

@Data
public class CLIArguments {
    private final String dictionaryFilePath;
    private final String phoneNumberFilePath;
}
