package com.singletonsd.oraclecodechallenge.services;

import com.singletonsd.oraclecodechallenge.models.CLIArguments;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.cli.*;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ArgumentParserService {

    private final Options options;
    private final Option dictionary;
    private final Option phoneNumber;
    private final Option help;

    public ArgumentParserService() {
        options = new Options();

        dictionary = new Option("d", "dictionary", true, "dictionary file path");
        dictionary.setRequired(false);
        options.addOption(dictionary);

        phoneNumber = new Option("f", "file", true, "phone numbers file path");
        phoneNumber.setRequired(false);
        options.addOption(phoneNumber);

        help = new Option("h", "help", false, "available options");
        phoneNumber.setRequired(false);
        options.addOption(help);
    }

    public CLIArguments parse(String[] args) {
        try {
            CommandLine cmd = new DefaultParser().parse(options, args);
            if(cmd.hasOption(help.getOpt())) {
                printHelp();
                return null;
            }
            return new CLIArguments(cmd.getOptionValue(dictionary.getOpt()),
                    cmd.getOptionValue(phoneNumber.getOpt()));
        } catch (ParseException e) {
            log.error("Error when parsing options: {}", e.getMessage());
            printHelp();
        }
        return null;
    }

    private void printHelp() {
        new HelpFormatter().printHelp("utility-name", options);
    }
}
