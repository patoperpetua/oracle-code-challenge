package com.singletonsd.oraclecodechallenge.services;

import com.singletonsd.oraclecodechallenge.exceptions.NotANumberException;
import com.singletonsd.oraclecodechallenge.exceptions.OutOfBoundNumberException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PhoneDigitTranslateService {
    private final String[] numberToCharMap;

    public PhoneDigitTranslateService() {
        this.numberToCharMap = new String[10];
        numberToCharMap[0] = "\0";
        numberToCharMap[1] = "\0";
        numberToCharMap[2] = "ABC";
        numberToCharMap[3] = "DEF";
        numberToCharMap[4] = "GHI";
        numberToCharMap[5] = "JKL";
        numberToCharMap[6] = "MNO";
        numberToCharMap[7] = "PQRS";
        numberToCharMap[8] = "TUV";
        numberToCharMap[9] = "WXYZ";
    }

    public List<String> translatePhoneDigit(String number) throws NotANumberException {
        List<String> result = new ArrayList<>();
        try {
            for (char character : number.toCharArray())
                result.add(translatePhoneDigit(character));
        } catch (Exception e) {
            throw new NotANumberException();
        }
        return result;
    }

    public String translatePhoneDigit(char number) throws NotANumberException {
        try {
            return translatePhoneDigit(Integer.parseInt(String.valueOf(number)));
        } catch (Exception e) {
            throw new NotANumberException();
        }
    }

    public String translatePhoneDigit(int number) throws OutOfBoundNumberException {
        if(number < 0 || number > 9)
            throw new OutOfBoundNumberException();
        return numberToCharMap[number];
    }
}
