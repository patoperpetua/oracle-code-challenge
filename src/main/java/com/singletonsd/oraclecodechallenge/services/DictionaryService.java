package com.singletonsd.oraclecodechallenge.services;

import com.singletonsd.oraclecodechallenge.exceptions.FileNotFoundException;
import com.singletonsd.oraclecodechallenge.exceptions.NotAFileException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class DictionaryService {

    private final String DEFAULT_DICTIONARY_RESOURCE_PATH = "/default_dictionary.txt";
    private final String DEFAULT_DICTIONARY_PATH = "dictionary-default.txt";

    private String currentDictionaryPath = DEFAULT_DICTIONARY_PATH;

    public DictionaryService() throws IOException {
        createFile();
    }

    private void createFile() throws IOException {
        InputStream initialStream = getClass().getResourceAsStream(DEFAULT_DICTIONARY_RESOURCE_PATH);
        File targetFile = new File(DEFAULT_DICTIONARY_PATH);
        java.nio.file.Files.copy(initialStream, targetFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
        initialStream.close();
    }

    public void readDictionary(String dictionaryPath) throws FileNotFoundException, NotAFileException {
        if(dictionaryPath == null || dictionaryPath.isEmpty()) {
            dictionaryPath = DEFAULT_DICTIONARY_PATH;
            log.info("Using default dictionary under {}", dictionaryPath);
        }
        File file = new File(dictionaryPath);
        if(!file.exists()) {
            log.error("Dictionary not found at {}", dictionaryPath);
            throw new FileNotFoundException();
        }
        if(!file.isFile()) {
            log.error("Dictionary provided is not a file {}", dictionaryPath);
            throw new NotAFileException();
        }
        log.info("Dictionary path successfully set to {}", dictionaryPath);
        currentDictionaryPath = dictionaryPath;
    }

    public void setDefaultDictionary(){
        this.currentDictionaryPath = DEFAULT_DICTIONARY_RESOURCE_PATH;
    }

    public List<String> getContainingWords(String word) throws IOException {
        return Files.lines(Paths.get(currentDictionaryPath))
            .filter(line -> line.toUpperCase().contains(word.toUpperCase()))
            .collect(Collectors.toList());
    }

    public List<String> getContainingWords(List<String> words) throws IOException {
        words.replaceAll(String::toUpperCase);
        return Files.lines(Paths.get(currentDictionaryPath))
                .filter(o -> words.stream().anyMatch(o.toUpperCase()::contains))
                .collect(Collectors.toList());
    }

    public List<String> getMatchingWords(String letters) throws IOException {
        return Files.lines(Paths.get(currentDictionaryPath))
                .filter(line -> line.equalsIgnoreCase(letters))
                .collect(Collectors.toList());
    }

    public List<String> getMatchingWords(List<String> words) throws IOException {

        return Files.lines(Paths.get(currentDictionaryPath))
                .filter(o -> words.stream().anyMatch(o::equalsIgnoreCase))
                .collect(Collectors.toList());
    }
}
