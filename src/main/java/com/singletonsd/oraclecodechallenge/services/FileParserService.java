package com.singletonsd.oraclecodechallenge.services;

import com.singletonsd.oraclecodechallenge.exceptions.FileNotFoundException;
import com.singletonsd.oraclecodechallenge.exceptions.FileTooLargeException;
import com.singletonsd.oraclecodechallenge.exceptions.NotAFileException;
import org.springframework.stereotype.Service;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

@Service
public class FileParserService {

    public List<String> parseFile(String filePath) throws Exception {
        if(filePath == null)
            throw new FileNotFoundException();
        File file = new File(filePath);
        if(!file.exists())
            throw new FileNotFoundException();
        if(!file.isFile())
            throw new NotAFileException();
        if(file.length() > 5 * 1024)
            throw new FileTooLargeException();
        return Files.readAllLines(Paths.get(filePath), StandardCharsets.US_ASCII);
    }
}
