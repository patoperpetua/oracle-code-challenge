package com.singletonsd.oraclecodechallenge.services;

import org.springframework.stereotype.Service;

@Service
public class TextSanitizerService {

    public String sanitizeText(String line) {
        return line
                .trim()
                .toUpperCase()
                .replaceAll("[^a-zA-Z]+\\.?", "");
    }

    public String sanitizeNumber(String line) {
        return line
                .trim()
                .replaceAll("[^0-9]+", "");
    }
}
