package com.singletonsd.oraclecodechallenge.services;

import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service
public class LetterCombinationService {

    public List<String> generateCombinations(List<String> lettersOfDigit) {
        List<String> ans = new ArrayList<>();
        if(lettersOfDigit == null || lettersOfDigit.size() == 0)
            return ans;
        int len = lettersOfDigit.size();
        depthFirstSearch(0, len, new StringBuilder(), ans, lettersOfDigit);
        return ans;
    }

//  https://en.wikipedia.org/wiki/Depth-first_search
    private void depthFirstSearch(int pos, int len, StringBuilder sb, List<String> ans, List<String> lettersOfDigit) {
        if (pos == len)
            ans.add(sb.toString());
        else {
            char[] letters = lettersOfDigit.get(pos).toCharArray();
            for (char letter : letters)
                depthFirstSearch(pos + 1, len, new StringBuilder(sb).append(letter), ans, lettersOfDigit);
        }
    }
}
