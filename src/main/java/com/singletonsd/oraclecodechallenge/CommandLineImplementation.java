package com.singletonsd.oraclecodechallenge;

import com.singletonsd.oraclecodechallenge.exceptions.FileNotFoundException;
import com.singletonsd.oraclecodechallenge.exceptions.NotAFileException;
import com.singletonsd.oraclecodechallenge.exceptions.NotANumberException;
import com.singletonsd.oraclecodechallenge.models.CLIArguments;
import com.singletonsd.oraclecodechallenge.services.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

@Slf4j
@Component
@AllArgsConstructor
@SpringBootApplication
public class CommandLineImplementation implements CommandLineRunner {

    private final ArgumentParserService argumentParserService;
    private final DictionaryService dictionaryService;
    private final FileParserService fileParserService;
    private final LetterCombinationService letterCombinationService;
    private final PhoneDigitTranslateService phoneDigitTranslateService;
    private final TextSanitizerService textSanitizerService;

    @Override
    public void run(String... arguments) {
        log.info("CLI running...");
        CLIArguments args = argumentParserService.parse(arguments);

        if(args == null) {
            log.info("No valid arguments provided, switching to STDIN mode");
            runStdinMode();
            return;
        }

        if(args.getDictionaryFilePath() == null)
            log.info("Dictionary not provided, using the default");
        else {
            log.info("Got dictionary file at: {}", args.getDictionaryFilePath());
            try {
                dictionaryService.readDictionary(args.getDictionaryFilePath());
            } catch (FileNotFoundException | NotAFileException e) {
                System.exit(1);
            }
        }

        if(args.getPhoneNumberFilePath() == null) {
            log.info("Phone numbers file not provided, switching to STDIN mode");
            runStdinMode();
        }
        else {
            log.info("Got phone numbers file at: {}", args.getPhoneNumberFilePath());
            List<String> phoneNumbers = null;
            try {
                phoneNumbers = fileParserService.parseFile(args.getPhoneNumberFilePath());
            } catch (Exception e) {
                System.exit(1);
            }
            for (String phoneNumber: phoneNumbers) {
                processPhoneNumber(phoneNumber);
            }
        }
    }

    private void processPhoneNumber(String phoneNumber) {
        try {
            String sanitizedNumber = textSanitizerService.sanitizeNumber(phoneNumber);
            log.debug("working with number {}", sanitizedNumber);
            List<String> letters = phoneDigitTranslateService.translatePhoneDigit(sanitizedNumber);
            List<String> combinations = letterCombinationService.generateCombinations(letters);
            log.debug("Found {} combinations", combinations.size());
            List<String> foundWords = dictionaryService.getMatchingWords(combinations);
            log.debug("Found {} matching words", foundWords.size());
            foundWords.forEach((n) -> log.info(textSanitizerService.sanitizeText(n)));
        } catch (NotANumberException | IOException e) {
            log.error("Phone Number: {}, could not be parsed", phoneNumber);
            log.error(e.getMessage());
        }
    }

    private void runStdinMode(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Insert a phone number");
        String phoneNumber = scanner.next();
        scanner.close();
        processPhoneNumber(phoneNumber);
    }
}
